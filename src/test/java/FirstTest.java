import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import javax.swing.text.Element;
import java.rmi.registry.LocateRegistry;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class FirstTest {

    @Test
    public void firstTest() {
        System.setProperty("webdriver.chrome.driver", "/Users/Admin/IdeaProjects/chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://www.google.com/webhp?hl=en&sa=X&ved=0ahUKEwjZj-PF6YPpAhUCwsQBHcgfBZcQPAgH");
        WebElement search = driver.findElement(By.name("q"));
        search.sendKeys("Netcracker");
        search.submit();
        WebElement wikipedia = driver.findElement(By.partialLinkText("Wikipedia"));
        wikipedia.click();
        WebElement link = driver.findElement(By.linkText("www.Netcracker.com"));
        assertEquals("http://www.netcracker.com/", link.getAttribute("href"));
        WebElement linkfirst = driver.findElement(By.xpath("//table[@class = 'infobox vcard']//a[text()='www.Netcracker.com']"));
        assertEquals("http://www.netcracker.com/", linkfirst.getAttribute("href"));
        driver.quit();
    }

}
